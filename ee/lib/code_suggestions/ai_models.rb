# frozen_string_literal: true

module CodeSuggestions
  module AiModels
    VERTEX_AI = :vertex_ai
    ANTHROPIC = :anthropic
  end
end
